# BReak Timing

- Tea Break : 11:00AM (15 Minutes)
- Lunch Break : 01:00PM (45 Minutes)
- Tea Break : 03:30PM (15 Minutes)

# JavaScript

- Dynamic Language
- Loosly Typed Language
- Cross Platform Apps (Web, Mobile, Hybrid)
- Scripting language
- OOPS Concepts (ES2015+ / ES6)
- Asynchronous / Non-blocking
- Single threaded
- Full Stack Apps (MEAN / MERN)
- Runs on both Client (Browser) as well as Server (Node Runtime Environment)

# JavaScript Engine

- Creational Phase - memory is allocated
- Execution Phase - assignments / return statements

# DataTypes in JavaScript

- Primitive Types - String, Number, Boolean
- Reference Types - Object, Function, Array, Date

- Typescript - Strictly typed language
- unknown
- any
- void
- never
- null
- undefined
- tuple
- enum
- Custom Types -> type keyword, interface, classes

# TypeScript Compiler

> npm install typescript -g

> tsc --version

> tsc filename.ts

# Generate tsconfig.json file

> tsc --init

rootDir : "./src",

outDir: "./build"

tsc

# Generate package.json file

> npm init -y

> npm run compile:watch

# ES6+ features

- Block scope variables - let / const
- Template Literals - back tick
- Destructuring - Object / Array
- Rest / Spread
- Promises
- Arrow Function

# Promise

- Pending : until the promise settled
- Settled :
  -> Success - Resolve
  -> Failure - Reject

- Producer : Promise Constructor
- Consumer :
  -> then().catch()
  -> Async...await

# Promise API Static Methods

- resolve -> immediately resolved
- reject -> immediately rejected
- race - returns the first settled promise whether resolve or reject
- any -> returns the first successfully resolved promise
- all -> all or nothing
- allSettled -> all the promises result whether rejected or resolved

# Classes

- Access Modifiers
  > public, private, protected, readonly
- Accessors / Mutators (getter / setter)
- Method Overriding - getDetails()
- Constructor Injection
- Static Property
- Static Method
- Extending Classes
- Abstraction

  > hiding the lower level details and exposing the functionality
  > Abstract Classes
  > -> Can have at least one or more abstract methods
  > -> Can not instantiate
  > -> Can also have the method implementation
  > -> Always extended

  e.g

  - Book -> title, author, getTitle(), getAuthor()
  - EPUB -> title, author, getTitle(), getAuthor(), bookType()
  - Kindle -> title, author, getTitle(), getAuthor(), bookType()

  > Interfaces
  > -> Contract between declaration and definition (b/w two parties)
  > -> Always implemented
  > -> Can only have abstract method (method definition)

# Decorators -> @expression

# Module System

- CommonJS : by default to Node Runtime
- ECMAScript Modules (ESM) : by default to Client (Browser)
- AMD : Asynchronous Module Definition
- UMD : Universal Module Definition
