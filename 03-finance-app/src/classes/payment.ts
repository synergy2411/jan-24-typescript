import { HasFormatter } from "../interface/has-formater";

export class Payment implements HasFormatter {
  constructor(
    private title: string,
    private amount: number,
    private createdAt: Date
  ) {}
  format(): string {
    const year = this.createdAt.getFullYear();
    const day = this.createdAt.toLocaleString("en-US", { day: "numeric" });
    const month = this.createdAt.toLocaleString("en-US", { month: "long" });
    return `You created payment for ${this.title} with an amount INR ${this.amount}$ on dated ${month} ${day}, ${year}`;
  }
}
