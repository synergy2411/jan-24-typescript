import { HasFormatter } from "../interface/has-formater";

export class ListTemplate {
  constructor(private listContainer: HTMLUListElement) {}

  render(paymentType: string, doc: HasFormatter) {
    const liElement = document.createElement("li") as HTMLLIElement;

    liElement.innerHTML = `
              <h1>${paymentType.toUpperCase()}</h1>
              <p>${doc.format()}</p>
        `;

    liElement.classList.add("list-group-item");

    this.listContainer.appendChild(liElement);
  }
}
