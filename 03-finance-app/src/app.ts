import { Invoice } from "./classes/invoice.js";
import { ListTemplate } from "./classes/list-template.js";
import { Payment } from "./classes/payment.js";
import { HasFormatter } from "./interface/has-formater.js";

window.onload = function () {
  // Input Fields
  const paymentType = document.getElementById(
    "paymentType"
  ) as HTMLSelectElement;
  const amount = document.getElementById("amount") as HTMLInputElement;
  const createdAt = document.getElementById("createdAt") as HTMLInputElement;
  const title = document.getElementById("title") as HTMLInputElement;

  // Buttons
  const btnAdd = document.getElementById("btnAdd") as HTMLButtonElement;
  const btnReset = document.getElementById("btnReset") as HTMLButtonElement;

  // List Container
  const listContainer = document.getElementById(
    "list-container"
  ) as HTMLUListElement;

  let listTemplate = new ListTemplate(listContainer);

  btnAdd.addEventListener("click", function (e: Event) {
    e.preventDefault();

    let doc: HasFormatter;

    let parsedAmount = Number(amount.value);
    let parsedDate = new Date(createdAt.value);

    if (paymentType.value === "invoice") {
      doc = new Invoice(title.value, parsedAmount, parsedDate);
    } else {
      doc = new Payment(title.value, parsedAmount, parsedDate);
    }

    listTemplate.render(paymentType.value, doc);
    resetFields();
  });

  btnReset.addEventListener("click", function (e: Event) {
    e.preventDefault();
    resetFields();
  });

  function resetFields() {
    title.value = "";
    amount.value = "";
    createdAt.value = "";
  }
};
