import { Invoice } from "./classes/invoice.js";
import { ListTemplate } from "./classes/list-template.js";
import { Payment } from "./classes/payment.js";
window.onload = function () {
    // Input Fields
    const paymentType = document.getElementById("paymentType");
    const amount = document.getElementById("amount");
    const createdAt = document.getElementById("createdAt");
    const title = document.getElementById("title");
    // Buttons
    const btnAdd = document.getElementById("btnAdd");
    const btnReset = document.getElementById("btnReset");
    // List Container
    const listContainer = document.getElementById("list-container");
    let listTemplate = new ListTemplate(listContainer);
    btnAdd.addEventListener("click", function (e) {
        e.preventDefault();
        let doc;
        let parsedAmount = Number(amount.value);
        let parsedDate = new Date(createdAt.value);
        if (paymentType.value === "invoice") {
            doc = new Invoice(title.value, parsedAmount, parsedDate);
        }
        else {
            doc = new Payment(title.value, parsedAmount, parsedDate);
        }
        listTemplate.render(paymentType.value, doc);
        resetFields();
    });
    btnReset.addEventListener("click", function (e) {
        e.preventDefault();
        resetFields();
    });
    function resetFields() {
        title.value = "";
        amount.value = "";
        createdAt.value = "";
    }
};
