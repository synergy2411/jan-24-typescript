export class ListTemplate {
    constructor(listContainer) {
        this.listContainer = listContainer;
    }
    render(paymentType, doc) {
        const liElement = document.createElement("li");
        liElement.innerHTML = `
              <h1>${paymentType.toUpperCase()}</h1>
              <p>${doc.format()}</p>
        `;
        liElement.classList.add("list-group-item");
        this.listContainer.appendChild(liElement);
    }
}
