export class Payment {
    constructor(title, amount, createdAt) {
        this.title = title;
        this.amount = amount;
        this.createdAt = createdAt;
    }
    format() {
        const year = this.createdAt.getFullYear();
        const day = this.createdAt.toLocaleString("en-US", { day: "numeric" });
        const month = this.createdAt.toLocaleString("en-US", { month: "long" });
        return `You created payment for ${this.title} with an amount INR ${this.amount}$ on dated ${month} ${day}, ${year}`;
    }
}
