// ---------------
// GENERICS - Placeholder for Types
// ---------------
let firstResource = {
    resourceId: 109,
    resourceName: "Resource 01",
};
// EXTENDING THE GENERICS
function demoGenerics(val) {
    return val;
}
const stringVal = demoGenerics("Hello World");
console.log(stringVal.length);
const numberVal = demoGenerics([101, 102, 103, 104]);
console.log(numberVal);
const todoVal = demoGenerics({ id: 101, label: "Grocery", length: 99 });
console.log(todoVal);
