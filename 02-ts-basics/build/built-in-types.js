// Explicit Typing
// let x: string;
// x = "Hello Again!";
// Implicit Typing (Type inference)
// let y = 201;
// console.log(y);
// let z: boolean;
// z = true;
// if (z) {
//   console.log("Z is true");
// }
// let x: string;
// x = 201;
// x = true;
// x = "Hello Again!";
// x = 201;
// console.log(x);
// --------------------
// Types in TypeScript
// --------------------
let varString = "Hello World";
let varNumber = 201;
let varBoolean = true;
let varDate = new Date("Deec 21, 2023");
let varObject = { name: "Foo" };
let varFunction = function () {
    console.log("Function");
};
let varStringArray1 = ["John", "Jennt", "James"];
let varStringArray2 = ["John", "Jennt", "James"];
let varNumberArray1 = [99, 98, 97, 100];
let varNumberArray2 = [99, 98, 97, 100];
// Tuple : fixed length with different datatypes
let varTuple = [101, "John", true];
// Union Type
let varAge;
varAge = "Thirty-two";
varAge = 32;
// Enum Type
var Color;
(function (Color) {
    Color[Color["RED"] = 99] = "RED";
    Color[Color["GREEN"] = 100] = "GREEN";
    Color[Color["BLUE"] = 101] = "BLUE";
})(Color || (Color = {}));
let varFavColor = Color.RED;
varFavColor = Color.GREEN;
varFavColor = Color.BLUE;
console.log(varFavColor);
// void
// function noReturn(): void {
//   console.log("Function Called");
// }
// console.log(noReturn());
// unknown
let varUnknown;
varUnknown = "Hello";
let varStr;
if (typeof varUnknown === "string") {
    varStr = varUnknown;
}
varUnknown = 201;
let varNum;
if (typeof varUnknown === "number") {
    varNum = varUnknown;
}
let varUnknown2;
let varNum3;
if (typeof varUnknown2 === "number") {
    varNum3 = varUnknown2;
}
// any
let varAny;
varAny = "Hello";
let varStr2;
varStr2 = varAny;
varAny = 31;
let varNum2;
varNum2 = varAny;
varAny = false;
varNum2 = varAny;
let varAny2;
varAny2 = "Hola";
let varBoolean2;
varBoolean2 = varAny2;
// null
let username;
username = null;
username = "John Doe";
let user;
user = {
    name: "John",
};
console.log(user.name);
// NEVER
function throwError() {
    throw new Error("Always throw the error");
}
export {};
