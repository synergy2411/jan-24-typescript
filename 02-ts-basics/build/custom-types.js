// Custom Type
let varMyType;
varMyType = "Hello";
varMyType = 199;
let user1;
user1 = {
    id: "E001",
    name: "John Doe",
    age: 32,
};
let user2 = {
    id: "E002",
    name: "Jenny",
    age: 28,
    isAdmin: true,
};
let newTodo = {
    id: "T001",
    label: "to buy some jeans",
    getDetails: function () {
        return this.label;
    },
};
// CLASS
class Foo {
}
let f;
f = new Foo();
