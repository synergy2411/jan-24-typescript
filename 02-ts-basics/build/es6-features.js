// BLOCK LEVEL SCOPE
// - restricting the scope of a variable to the nearest block
class Kindle {
    //   private title: string;
    //   private author: string;
    //   constructor(title: string, author: string) {
    //     this.title = title;
    //     this.author = author;
    //   }
    constructor(title, author) {
        this.title = title;
        this.author = author;
    }
    getTitle() {
        return this.title;
    }
    getAuthor() {
        return this.author;
    }
}
let tomAndJerry = new Kindle("Tom And Jerry", "New Author");
console.log(tomAndJerry.getTitle());
console.log(tomAndJerry.getAuthor());
