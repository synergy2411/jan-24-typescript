// ----------------
// DECORATORS - FOR META-PROGRAMMING
// ----------------
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
// VARIOUS DECORATOR SIGNATURES
// ----------------------------
// function ClassDecorator(target: Function) { }
// function PropertyDecorator(target: any, propertyName: string) { }
// function MethodDecorator(target: any, methodName: string, propertyDescriptor: PropertyDescriptor) { }
// function AccessorDecorator(target: any, propertyName: string, propertyDescriptor: PropertyDescriptor) { }
// function ParameterDecorator(target: any, propertyName : string, parameter : any) { }
// @ClassDecorator
// class Employee {
//     @PropertyDecorator
//     private empId: string;
//     private _deptt: string;
//     constructor(empId: string, private name: string) {
//         this.empId = empId
//     }
//     @MethodDecorator
//     getDetails() {
//         return this.empId + " - " + this.name;
//     }
//     changeName(@ParameterDecorator name : string) { }
//     @AccessorDecorator
//     get department() {
//         return this._deptt;
//     }
// }
// function printPropertyName(target: any, propertyName: string) {
//   console.log("Property Name - ", propertyName);
//   Object.defineProperty(target, propertyName, {
//     configurable: false,
//     get: () => "John Doe",
//   });
// }
// class Test {
//   @printPropertyName
//   username: string;
// }
// let t = new Test();
// console.log(t.username);
// let friends = ;
function allowedFriends(friends) {
    return function (target, property) {
        let currentValue = target[property];
        Object.defineProperty(target, property, {
            set: (friend) => {
                if (!friends.includes(friend)) {
                    return;
                }
                currentValue = friend;
            },
            get: () => currentValue,
        });
    };
}
class Party {
}
__decorate([
    allowedFriends(["Monica", "Joey", "Ross"]),
    __metadata("design:type", String)
], Party.prototype, "friend", void 0);
let p1 = new Party();
p1.friend = "Joey";
console.log(p1.friend);
p1.friend = "Rachel";
console.log(p1.friend);
p1.friend = "Ross";
console.log(p1.friend);
