var Fortune;
(function (Fortune) {
    function getFortune() {
        return "Today is your lucky day!";
    }
    Fortune.getFortune = getFortune;
    function getDailyQuote() {
        return "Run 5 miles today!";
    }
    Fortune.getDailyQuote = getDailyQuote;
    class Person {
        constructor(name) {
            this.name = name;
        }
        getName() {
            return this.name;
        }
    }
    Fortune.Person = Person;
})(Fortune || (Fortune = {}));
let john = new Fortune.Person("John Doe");
console.log(john.getName());
console.log(Fortune.getDailyQuote());
