namespace Fortune {
  export function getFortune() {
    return "Today is your lucky day!";
  }
  export function getDailyQuote() {
    return "Run 5 miles today!";
  }

  export class Person {
    constructor(private name: string) {}
    getName() {
      return this.name;
    }
  }
}

let john = new Fortune.Person("John Doe");
console.log(john.getName());

console.log(Fortune.getDailyQuote());
