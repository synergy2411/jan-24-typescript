// BLOCK LEVEL SCOPE
// - restricting the scope of a variable to the nearest block

// LET / CONST

// VAR - restricted by the functions

// function demo(arr: Array<number>) {
//   if (arr.length > 2) {
//     let LOAD = "LOADING";
//     console.log(FLASH);
//   } else {
//     let FLASH = "FLASHING";
//   }
// }

// demo([99, 98, 97]);

// const username = "John Doe";

// username = "Jenny";

// const user = {
//   name: "John Doe",
// };

// user.name = "Jenny";

// console.log(user); // "Jenny"

// user = {
//     name : "James"
// }

// const friends = ["Monica", "Rachel", "Joey"];

// friends.push("Ross");

// console.log(friends.length); // ?

// ERROR - CAN'T REASSIGN TO CONSTANTS
// friends = ["Monica", "Rachel", "Joey", "Ross"];

// TEMPLATE LITERALS
// BACK TICK (` `)
// - EMBED THE VARIABLES WITHIN STRING WITHOUT (+)
// - MULTILINE STRINGS WITHOUT (\n)

// let username = "John Doe";
// let userAge = 32;

// let greet = `Hello ${username}!!
// I'm ${userAge} years old!
// `;

// console.log(greet);

// ARROW FUNCTION =>
// - short and clean syntax
// - great for one liner function

// const sum = (n1: number, n2: number) => n1 + n2;

// console.log("Sum : ", sum(2, 4));

// RESTRCITIONS ON ARROW FUNCTIONS
// - don't have their own 'this' keyword
// - don't have 'arguments' keyword
// - can't call arrow function with 'new' operator

// let user = {
//   firstName: "John",
//   lastName: "Doe",
//   getFullName: function () {
//     let that = this;
//     return function () {
//       return that.firstName + " " + that.lastName;
//     };
//   },
// };
// let user = {
//   firstName: "John",
//   lastName: "Doe",
//   getFullName: function () {
//     return () => this.firstName + " " + this.lastName;
//   },
// };

// let nestedFn = user.getFullName();

// console.log(nestedFn());

// --------
// const demoArgs = () => {
//   console.log(arguments);
// };

// demoArgs(123, "John Doe", true);

// --------

// const Person = (email: string, age: number) => {
//   this.email = email;
//   this.age = age;
// };

// let foo = new Person("foo@test", 32);

// console.log(foo.email);

// DESTRUCTURING : unpacking the collection

// let fruits = ["Apple", "Banana", "Oranges"];

// let [f1, f2, f3] = fruits;

// console.log(f3); // "Oranges"

// let a = 10;
// let b = 15;

// [a, b] = [b, a];

// console.log(b); // ?

// OBJECT DESTRUCTURING

// let user = {
//   username: "John Doe",
//   email: "john@test",
//   age: 32,
//   address: {
//     street: "201, Main Road",
//     city: "Pune",
//   },
//   friends: ["Ross", "Monica", "Rachel"],
// };

// let {
//   email,
//   age,
//   username,
//   address: { city, street },
//   friends: [f1, f2, f3],
// } = user;

// console.log(username, age, street, f3);

// Property Alias
// let userOne = {
//   email: "one@test",
//   age: 32,
// };

// let userTwo = {
//   email: "two@test",
//   isAdmin: true,
// };

// let { email: userOneEmail, age } = userOne;
// let { email: userTwoEmail, isAdmin } = userTwo;

// console.log(userOneEmail, userTwoEmail, age, isAdmin);

// let user = {
//   uname: "John Doe",
//   address: {
//     street: "201 Main Road, Wakad",
//     city: "Pune",
//   },
//   friends: ["f1", "f2", "f3"],
// };

// // Shallow Copy
// let { uname, address, friends } = user;

// friends.push("f4");

// console.log(user.friends.length); //4

// address.city = "Bengaluru";

// uname = "Jenny";

// console.log(user.uname); // John Doe

// console.log(user.address); // ?

// DEEP VS SHALLOW

// let userOne = {
//   name: "John",
// };

// // DEEP COPY
// let userTwo = JSON.parse(JSON.stringify(userOne));

// userTwo.name = "Jenny";

// console.log(userOne.name);

// --------------
// REST & SPREAD (...)
// --------------

// REST -> create collection (Array) from individual elements
// - only available in function arguments list as last argument

// function demoRest(label, ...args) {
//   console.log(args[0]); // ?
// }

// demoRest("foo@test")
// demoRest("foo@test", 32)
// demoRest("foo@test", 32, true);

// function mystry(...x) {
//   return x;
// }

// const whoAmI = mystry(99, 98, 100, 101);

// console.log(whoAmI); // ?

// SPREAD - create individual elements from a collection

// let arr = [3, 4, 5];

// let moreValue = [1, 2, ...arr];

// console.log(moreValue);

// let user = {
//   uname: "John",
//   email: "john@test.com",
//   company: "Synechron",
// };

// let userTwo = {
//   ...user,
//   uname: "Jenny",
//   age: 32,
// };

// console.log(userTwo); //

// let userOne = {
//   company: "Synechron",
//   isAdmin: true,
// };

// let userTwo = {
//   city: "Pune",
//   company: "XYZ",
// };

// const newUser = Object.assign({}, userOne, userTwo);

// console.log(newUser);

// ---------
// PROMISE
// ---------

// const promiseProducer = (arr) => {
//   const promise = new Promise((resolve, reject) => {
//     setTimeout(() => {
//       if (arr.length > 2) {
//         resolve({ message: "SUCCESS" });
//       } else {
//         reject(new Error("Something went wrong"));
//       }
//     }, 2000);
//   });

//   return promise;
// };

// ASYNC...AWAIT

// const promiseConsumer = async () => {
//   let arr = [100, 101];
//   try {
//     const result = await promiseProducer(arr);
//     console.log("ASYNC RESULT : ", result);
//   } catch (err) {
//     console.error(err);
//   }
// };

// THEN & CATCH
// const promiseConsumer = () => {
//   let arr = [99, 98, 97, 101];

//   promiseProducer(arr)
//     .then((result: { message: string }) => {
//       console.log("FIRST THEN - ", result);
//       return result.message;
//     })
//     .then(
//       (msg) => {
//         console.log("SECOND THEN - ", msg);
//         throw new Error("From Second");
//         return msg.toLowerCase();
//       },
//       (err) => console.error("ERROR -> ", err)
//     )
//     .then((msgInCaptial) => console.log("THIRD THEN- ", msgInCaptial))
//     .catch((err) => console.error(err));
// };

// promiseConsumer();

// FETCHING DATA FROM JSON TYPICODE

// const btnFetch = document.querySelector("#btnFetch") as HTMLButtonElement;
// const container = document.querySelector("#container") as HTMLUListElement;

// btnFetch.addEventListener("click", async function () {
//   try {
//     const response = await fetch("https://jsonplaceholder.typicode.com/todos");
//     const result = await response.json();
//     console.log("RESULT : ", result);
//     result.forEach((todo) => {
//       const liElement = document.createElement("li");
//       liElement.innerHTML = todo.title.toUpperCase();
//       container.appendChild(liElement);
//     });
//   } catch (err) {
//     console.error(err);
//   }
// });

// PROMISE STATIC METHODS

// Promise.resolve("The Data").then(console.log);

// Promise.reject(new Error("Bad Request")).catch(console.error);

// const promiseOne = new Promise((resolve, reject) => {
//   setTimeout(() => resolve("Promise One"), 4000);
// });
// const promiseTwo = new Promise((resolve, reject) => {
//   setTimeout(() => reject("Promise Two"), 1000);
// });
// const promiseThree = new Promise((resolve, reject) => {
//   setTimeout(() => resolve("Promise Three"), 2000);
// });

// const allPromises = [promiseOne, promiseTwo, promiseThree];

// Promise.all(allPromises).then(console.log).catch(console.error);
// Promise.allSettled(allPromises).then(console.log).catch(console.error);
// Promise.race(allPromises).then(console.log).catch(console.error);
// Promise.any(allPromises).then(console.log).catch(console.error);

// DEFAULT PARAMETER

// function demoDefault(arr: string[] = []) {
//   //   const array = arr || [];
//   if (arr.length > 2) {
//     console.log("Greater then 2");
//   }
// }

// demoDefault(["A", "B", "C"]);

// function demoDefaultTwo(state: { label: string } = { label: "" }) {
//   console.log(state.label);
// }

// demoDefaultTwo({ label: "Supplied Label" });

// -------------
// CLASSES
// -------------

// class Person {
//   //   private name: string;
//   //   public age: number;
//   readonly ssn: string = "S001";

//   constructor(private name: string, public age: number) {
//     // this.name = name;
//     // this.age = age;
//   }

//   getDetails(): string {
//     return this.name + " - " + this.age;
//   }
// }

// const OFFERED_COURSES = ["ANGULAR", "REACT", "NODE", "JAVASCRIPT"];

// class Student extends Person {
//   private _courseName: string;

//   private static numberOfStudents: number = 0;

//   constructor(name: string, age: number, private studentId: string) {
//     super(name, age);
//     Student.numberOfStudents += 1;
//   }

//   get courseName() {
//     return this._courseName;
//   }

//   set courseName(course: string) {
//     if (!OFFERED_COURSES.includes(course)) {
//       return;
//     }
//       this._courseName = course;
//   }

//   // Override the base class method
//   getDetails(): string {
//     return this.studentId + " -> " + super.getDetails();
//   }

//   static getTotalStudent() {
//     return Student.numberOfStudents;
//   }
// }

// let john = new Student("John Doe", 23, "S001");

// console.log(john.getDetails());
// john.courseName = "REACT";

// console.log(john.courseName);

// john.courseName = "JAVA";

// console.log(john.courseName);

// console.log("Total Students : " + Student.getTotalStudent());

// let jenny = new Student("Jenny Alice", 24, "S002");

// console.log("Total Students : " + Student.getTotalStudent());

// let john = new Person("John Doe", 23);
// console.log(john.getDetails());

// console.log(john.age);
// console.log(john.name);
// console.log(john.ssn);
// john.ssn = "S010";
// console.log(john.ssn);

// --------------
// ABSTRACTION - ABSTRACT CLASS
// --------------

// abstract class Book {
//   private title: string;
//   private author: string;

//   constructor(title: string, author: string) {
//     this.title = title;
//     this.author = author;
//   }

//   getTitle() {
//     return this.title;
//   }

//   getAuthor() {
//     return this.author;
//   }

//   abstract getBookType(): string;
// }

// class EPUB extends Book {
//   private bookType: string = "EPUB";

//   constructor(title: string, author: string) {
//     super(title, author);
//   }

//   getBookType(): string {
//     return this.bookType;
//   }
// }

// class Kindle extends Book {
//   private bookType: string = "Kindle";

//   constructor(title: string, author: string) {
//     super(title, author);
//   }

//   getBookType(): string {
//     return this.bookType;
//   }
// }

// let shivaTheHero: Book = new EPUB("Shiva - The Hero", "Author 01");
// let tomAndJerry: Book = new Kindle("Tom & Jerry", "Author 02");

// console.log(shivaTheHero.getBookType());
// console.log(shivaTheHero.getTitle());
// console.log(shivaTheHero.getAuthor());

// console.log(tomAndJerry.getBookType());
// console.log(tomAndJerry.getTitle());
// console.log(tomAndJerry.getAuthor());

// --------------
// ABSTRACTION - INTERFACE
// --------------

interface IBook {
  getTitle: () => string;
  getAuthor: () => string;
}

class Kindle implements IBook {
  //   private title: string;
  //   private author: string;

  //   constructor(title: string, author: string) {
  //     this.title = title;
  //     this.author = author;
  //   }
  constructor(private title: string, private author: string) {}

  getTitle(): string {
    return this.title;
  }

  getAuthor(): string {
    return this.author;
  }
}

let tomAndJerry: IBook = new Kindle("Tom And Jerry", "New Author");

console.log(tomAndJerry.getTitle());
console.log(tomAndJerry.getAuthor());
