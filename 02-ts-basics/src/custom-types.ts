// Custom Type

// TYPE KEYWORD
type MyOwnType = string | number;

let varMyType: MyOwnType;

varMyType = "Hello";

varMyType = 199;

type MYComplexType = {
  id: string;
  name: string;
  age: number;
  isAdmin?: boolean;
};

let user1: MYComplexType;

user1 = {
  id: "E001",
  name: "John Doe",
  age: 32,
};

let user2: MYComplexType = {
  id: "E002",
  name: "Jenny",
  age: 28,
  isAdmin: true,
};

// INTERFACE

interface ITodo {
  id: string;
  label: string;
  getDetails: () => string;
}

let newTodo: ITodo = {
  id: "T001",
  label: "to buy some jeans",
  getDetails: function () {
    return this.label;
  },
};

// CLASS
class Foo {}

let f: Foo;

f = new Foo();
