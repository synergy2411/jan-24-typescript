// Explicit Typing

// let x: string;

// x = "Hello Again!";

// Implicit Typing (Type inference)

// let y = 201;

// console.log(y);

// let z: boolean;

// z = true;

// if (z) {
//   console.log("Z is true");
// }

// let x: string;

// x = 201;

// x = true;

// x = "Hello Again!";

// x = 201;

// console.log(x);

// --------------------
// Types in TypeScript
// --------------------

let varString: string = "Hello World";

let varNumber: number = 201;

let varBoolean: boolean = true;

let varDate: Date = new Date("Deec 21, 2023");

let varObject: object = { name: "Foo" };

let varFunction: Function = function () {
  console.log("Function");
};

let varStringArray1: Array<string> = ["John", "Jennt", "James"];
let varStringArray2: string[] = ["John", "Jennt", "James"];

let varNumberArray1: Array<number> = [99, 98, 97, 100];
let varNumberArray2: number[] = [99, 98, 97, 100];

// Tuple : fixed length with different datatypes
let varTuple: [number, string, boolean] = [101, "John", true];

// Union Type
let varAge: string | number;

varAge = "Thirty-two";

varAge = 32;

// Enum Type

enum Color {
  RED = 99,
  GREEN,
  BLUE,
}

let varFavColor: Color = Color.RED;
varFavColor = Color.GREEN;
varFavColor = Color.BLUE;

console.log(varFavColor);

// void

// function noReturn(): void {
//   console.log("Function Called");
// }

// console.log(noReturn());

// unknown

let varUnknown: unknown;

varUnknown = "Hello";

let varStr: string;

if (typeof varUnknown === "string") {
  varStr = varUnknown;
}

varUnknown = 201;

let varNum: number;

if (typeof varUnknown === "number") {
  varNum = varUnknown;
}

let varUnknown2: unknown;

let varNum3: number;

if (typeof varUnknown2 === "number") {
  varNum3 = varUnknown2;
}

// any

let varAny: any;

varAny = "Hello";

let varStr2: string;

varStr2 = varAny;

varAny = 31;

let varNum2: number;

varNum2 = varAny;

varAny = false;

varNum2 = varAny;

let varAny2: any;
varAny2 = "Hola";

let varBoolean2: boolean;

varBoolean2 = varAny2;

// null

let username: string | null;

username = null;

username = "John Doe";

let user: { name: string } | null;

user = {
  name: "John",
};

console.log(user.name);

// NEVER
function throwError(): never {
  throw new Error("Always throw the error");
}

export {};
