// ----------------
// DECORATORS - FOR META-PROGRAMMING
// ----------------

// VARIOUS DECORATOR SIGNATURES
// ----------------------------

// function ClassDecorator(target: Function) { }

// function PropertyDecorator(target: any, propertyName: string) { }

// function MethodDecorator(target: any, methodName: string, propertyDescriptor: PropertyDescriptor) { }

// function AccessorDecorator(target: any, propertyName: string, propertyDescriptor: PropertyDescriptor) { }

// function ParameterDecorator(target: any, propertyName : string, parameter : any) { }

// @ClassDecorator
// class Employee {

//     @PropertyDecorator
//     private empId: string;
//     private _deptt: string;

//     constructor(empId: string, private name: string) {
//         this.empId = empId
//     }

//     @MethodDecorator
//     getDetails() {
//         return this.empId + " - " + this.name;
//     }

//     changeName(@ParameterDecorator name : string) { }

//     @AccessorDecorator
//     get department() {
//         return this._deptt;
//     }
// }

// function printPropertyName(target: any, propertyName: string) {
//   console.log("Property Name - ", propertyName);
//   Object.defineProperty(target, propertyName, {
//     configurable: false,
//     get: () => "John Doe",
//   });
// }

// class Test {
//   @printPropertyName
//   username: string;
// }

// let t = new Test();

// console.log(t.username);

// let friends = ;

function allowedFriends(friends) {
  return function (target: any, property: string) {
    let currentValue = target[property];

    Object.defineProperty(target, property, {
      set: (friend) => {
        if (!friends.includes(friend)) {
          return;
        }
        currentValue = friend;
      },
      get: () => currentValue,
    });
  };
}

class Party {
  @allowedFriends(["Monica", "Joey", "Ross"])
  friend: string;
}

let p1 = new Party();

p1.friend = "Joey";

console.log(p1.friend);

p1.friend = "Rachel";
console.log(p1.friend);
p1.friend = "Ross";
console.log(p1.friend);
