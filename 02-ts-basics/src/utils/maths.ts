const sum = (n1: number, n2: number) => n1 + n2;

const mul = (n1: number, n2: number) => n1 * n2;

const square = (n1: number) => n1 * n1;

const rnd = Math.round(Math.random() * 100);

const getRandomNumber = () => rnd;

// Named Export
export { sum, mul, getRandomNumber };

// Default Export

export default square;
