// ---------------
// GENERICS - Placeholder for Types
// ---------------

// function addAtBeginningString(val: string, arr: string[]): string[] {
//   return [val, ...arr];
// }

// function addAtBeginningNumber(val: number, arr: number[]): number[] {
//   return [val, ...arr];
// }

// const modifiedStringArray = addAtBeginningString("Joey", ["Monica", "Ross"]);

// console.log(modifiedStringArray);

// const modifiedNumberArray = addAtBeginningNumber(99, [101, 103, 105]);

// console.log(modifiedNumberArray);

// METHOD OVERLOADING
// function addAtBeginning(val: string, arr: string[]): string[];
// function addAtBeginning(val: number, arr: number[]): number[];
// function addAtBeginning(val: any, arr: any[]): any[]{
//     return [val, ...arr]
// }

// const modifiedArray1 = addAtBeginning("Joey", ["Monica", "Rachel"])

// const modifiedArray2 = addAtBeginning(99, [101, 103, 105])

// GENERICS - FUNCTION

// function addAtBeginning<T>(val: T, arr: T[]): T[] {
//   return [val, ...arr];
// }

// const stringArray = addAtBeginning<string>("Joey", ["Monica", "Ross"]);

// const numberArray = addAtBeginning<number>(101, [99, 97, 95]);

// type Todo = {
//   id: string;
//   label: string;
// };

// let todo: Todo = { id: "T001", label: "buy some jeans" };

// let todoColl: Todo[] = [
//   { id: "T002", label: "shop for grocery" },
//   { id: "T003", label: "renew car insurance" },
// ];

// const todoArray: Todo[] = addAtBeginning<Todo>(todo, todoColl);

// console.log(todoArray);

// GENERICS - CLASSES

// class Server<T, K> {
//   private serverId: T;
//   private serverLocation: K;

//   constructor(serverId: T, location: K) {
//     this.serverId = serverId;
//     this.serverLocation = location;
//   }

//   getServerId(): T {
//     return this.serverId;
//   }
//   getServerLocation(): K {
//     return this.serverLocation;
//   }
// }

// let server1 = new Server<string, string>("S001", "Delhi");
// console.log(server1.getServerId());
// console.log(server1.getServerLocation());

// let server2 = new Server<number, string>(101, "Pune");
// console.log(server2.getServerId());
// console.log(server2.getServerLocation());

// GENERICS - INTERFACE

interface Resource<T, K> {
  resourceId: T;
  resourceName: K;
}

let firstResource: Resource<number, string> = {
  resourceId: 109,
  resourceName: "Resource 01",
};

// EXTENDING THE GENERICS
function demoGenerics<T extends { length: number }>(val: T): T {
  return val;
}

const stringVal = demoGenerics<string>("Hello World");
console.log(stringVal.length);

const numberVal = demoGenerics<Array<number>>([101, 102, 103, 104]);

console.log(numberVal);

interface Todo {
  id: number;
  label: string;
  length: number;
}

const todoVal = demoGenerics<Todo>({ id: 101, label: "Grocery", length: 99 });

console.log(todoVal);
